const express = require('express');
const mongoose = require('mongoose');
const app = express();

const dbCheckMiddleware = require("./middlewares/dbCheckMiddleware");

let isConnected = false;

const { port } = require('./config/server');
const dbConfig = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const bookRouter = require('./routers/noteRouter');
const { request, response } = require('express');

const dbConnectString = `mongodb+srv://HW2DBUser:${dbConfig.password}@nodejshw2notesapp.5royl.mongodb.net/${dbConfig.databaseName}?retryWrites=true&w=majority`
//`mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.databaseName}`
  mongoose.connect(dbConnectString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});


app.use(express.json());

app.use('/api', dbCheckMiddleware, userRouter);
app.use('/api', dbCheckMiddleware, authRouter);
app.use('/api', dbCheckMiddleware, bookRouter);
app.use('/', (request, response)=>{
  response.status(400).json({message:"Data or query path could not be found"});
})





app.listen(port, () => {
    console.log(`Server listens on ${port} port`);
});