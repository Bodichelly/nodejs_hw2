const mongoose = require('mongoose');
const Schema = mongoose.Schema;

module.exports = mongoose.model('note', new Schema({
    userId:{
        require: true,
        type: String
    },
    text: {
        required: true,
        type: String
    },
    createdDate: {
        required: true,
        type: String,
        format: Date,
    },
    completed:{
        required: true,
        type: Boolean
    }
}));

