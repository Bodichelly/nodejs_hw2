const e = require('express');
const User = require('../models/user');

const getUser = async(id)=>{
    try{
        if(!id){
            throw {
                    status: 500,
                    message: "Internal data lose"
            }
        }
        let user;
        try{
            user = await User.findById(id).lean().exec();
        }catch(e){
            throw {
                status: 400,
                message: JSON.stringify(e) || e+""
        }
        }
        
        if(!user){
            throw {
                status: 400,
                message: "Data not found"
            };
        }
        return user;
    }catch(e){
        throw e
    }
};
const getUserByUsernameAndPassword = async(username, password)=>{
    const query = {'username': username, 'password': password};
    try{
        if(!username || !password){
            throw {
                status: 500,
                message: "Internal data lose"
        }
        }
        let user;
        try{
            user = await User.findOne(query).lean().exec();
        }catch(e){
            throw {
                status: 400,
                message: JSON.stringify(e) || e+""
        }
        }
        
        if(!user){
            throw {
                status: 400,
                message: "Data not found"
            };
        }
        return user;
    }catch(e){
        throw e;
    }
};
const deleteUser = async(_id)=>{
    const query = {'_id': _id};
    try{
        if(!_id){
            throw {
                status: 500,
                message: "Internal data lose"
        }
        }
        try{
            await User.deleteOne(query);
        }catch(e){
            throw {
                status: 400,
                message: JSON.stringify(e) || e+""
        }
        }
        
        return true;
    }catch(e){
        throw e;
    }
};

const addUser = async(username, password)=>{
    
    const createdDate = new Date().toISOString();
    if(!createdDate){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const user = new User({username, password, createdDate});
    if(!user){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    try{
        await user.save();
        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};

const updateUser = async(_id, password)=>{
    if(!_id || !password){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const query = {'_id': _id};
    const update = {"password": password};
    const options = {
        returnOriginal: false,
    }
    try{
        await User.findOneAndUpdate(query, update, options);
        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};




exports.getUser = getUser;
exports.deleteUser = deleteUser;
exports.addUser = addUser;
exports.updateUser = updateUser;
exports.getUserByUsernameAndPassword = getUserByUsernameAndPassword;





