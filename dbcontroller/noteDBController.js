const Note = require('../models/note');
const user = require('../models/user');


const getNote = async(id)=>{
    if(!id){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    try{
        let note;
        try{
            note = await Note.findById(id).lean().exec();
        }catch(e){
            throw {
                status: 400,
                message: JSON.stringify(e) || e+""
        }
        }
        
        if(!note){
            throw {
                status: 400,
                message: "Data not found"
        }
        }
        return note;
    }catch(e){
        throw e;
    }
};

const getNotes = async(userId)=>{
    if(!userId){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const query = {'userId': userId};
    try{
        let note;
        try{
            note = await Note.find(query).lean().exec();
        }catch(e){
            throw {
                status: 400,
                message: JSON.stringify(e) || e+""
        }
        }
        
        if(!note){
            throw {
                status: 400,
                message: "Data not found"
        }
        }
        return note;
    }catch(e){
        throw e;
    }
};

const deleteNote = async(_id)=>{
    if(!_id){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const query = {'_id': _id};
    try{
        await Note.deleteOne(query).exec();

        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};

const addNote = async(userId, text)=>{
    const completed = false;
    const createdDate = new Date().toISOString();
    if(!createdDate){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    if(!userId || !text){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const note = new Note({userId, text, createdDate, completed});
    try{
        await note.save();
        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};

const updateNote = async(_id, text)=>{
    if(!_id || !text){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    const query = {'_id': _id};
    const update = {"text": text};
    const options = {
        returnOriginal: false,
    }
    try{
        await Note.findOneAndUpdate(query, update, options);
        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};

const checkNote = async(_id)=>{
    if(!_id){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    try{
        return await setCompleteStatusNote(_id, true);
    }catch (e){
        throw e;
    }
}

const uncheckNote = async(_id)=>{
    if(!_id){
        throw {
            status: 500,
            message: "Internal data lose"
    }
    }
    try{
        return await setCompleteStatusNote(_id, false);
    }catch (e){
        throw e;
    }
}

const setCompleteStatusNote = async(_id, status)=>{
    
    const query = {'_id': _id};
    const update = {"completed": status};
    const options = {
        returnOriginal: false,
    }
    try{
        await Note.findOneAndUpdate(query, update, options);
        return true;
    }catch(e){
        throw {
            status: 400,
            message: JSON.stringify(e) || e+""
    }
    }
};




exports.getNote_ = getNote;
exports.getNotes_ = getNotes;
exports.deleteNote_ = deleteNote;
exports.addNote_ = addNote;
exports.updateNote_ = updateNote;
exports.checkNote_ = checkNote;
exports.uncheckNote_ = uncheckNote;

