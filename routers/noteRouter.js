const express = require('express');
const router = express.Router();

const { addNote, getNotes, getNote, putNote, patchNote, deleteNote } = require('../controllers/noteController');

const authMiddleware = require('../middlewares/authMiddleware');

router.post('/notes', authMiddleware, addNote);

router.get('/notes', authMiddleware, getNotes);

router.get('/notes/:id', authMiddleware, getNote);

router.put('/notes/:id', authMiddleware, putNote);

router.patch('/notes/:id', authMiddleware, patchNote);

router.delete('/notes/:id', authMiddleware, deleteNote);

module.exports = router;