const auth = require("../config/auth");

const jwt = require('jsonwebtoken');

const { secret } = require('../config/auth');

module.exports = (request, response, next) => {
    const authHeader = request.headers['authorization'];

    if(!authHeader) {
        return response.status(401).json({status: 'No authorization header found'});
    }

    const [, jwtToken] = authHeader.split(' ');

    try {
        const user = jwt.verify(jwtToken, secret);
        request.body.userId = user._id;
        request.body.username = user.username;
        request.body.password = user.password;
        
        next();
    } catch (err) {
        response.setHeader("Content-Type", "application/json");
        return response.status(401).json({status: 'Invalid JWT'});
    }
};
