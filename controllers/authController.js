 const jwt = require('jsonwebtoken');
const {addUser, getUserByUsernameAndPassword} = require("../dbcontroller/userDBController");

const { secret } = require('../config/auth');
const { request, response } = require('express');
const sha256 = require('js-sha256').sha256;


module.exports.register = async (request, response) => {
    response.setHeader("Content-Type", "application/json");
    const { username, password } = request.body;
    if(!username || !password || username.length==0 || password.length==0){
        response.status(400).json({message: 'Invalid username or password'});
        return;
    }
    const passwordHash = sha256(password);
    try{
        await addUser(username, passwordHash);
        response.status(200).json({status: 'Success'});
    }catch(e){
        response.status(e.status).json({ message: e.message});
    }
};

module.exports.login = async (request, response) => {
    response.setHeader("Content-Type", "application/json");
    const { username, password } = request.body;
    if(!username || !password || username.length==0 || password.length==0){
        response.status(400).json({message: 'Invalid username or password'});
        return;
    }
    const passwordHash = sha256(password);
    try{
        const user = await getUserByUsernameAndPassword(username, passwordHash);
        if (!user ) {
            
            response.status(400).json({message: 'No user with such username and password found'});
        }else{
            response.status(200).json({jwt_token: jwt.sign(JSON.stringify(user), secret)});
        }
    }catch(e){
        response.status(e.status).json({ message: e.message});
    }
    
};



