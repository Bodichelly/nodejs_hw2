const { request } = require("express");
const {
  addNote_,
  getNote_,
  getNotes_,
  deleteNote_,
  checkNote_,
  uncheckNote_,
  updateNote_,
} = require("../dbcontroller/noteDBController");
const {getUserByUsernameAndPassword} = require("../dbcontroller/userDBController");

module.exports.addNote = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { username, password, text } = request.body;
  if(!username || !password || !text || username.length<4 || password.length<4 || text.length==0){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
    const user = await getUserByUsernameAndPassword(username, password);
    await addNote_(user._id, text);
    response.status(200).json({ message: "Success"});
    return;

  } catch (e) {
    response.status(e.status).json({ message: e.message});
    return;
  }
};

module.exports.getNotes = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { username, password } = request.body;
  if(!username || !password || username.length<4 || password.length<4){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
      const user = await getUserByUsernameAndPassword(username, password);
    const notes = await getNotes_(user._id);
    const notesResponseFormat = notes.map((note)=>{
      return {
        "_id": note._id,
        "userId": note.userId,
        "completed": note.completed,
        "text": note.text,
        "createdDate": note.createdDate
      }

    });
    response.status(200).json(notesResponseFormat);
  } catch (e) {
    response.status(e.status).json({ message: e.message});
  }
};

module.exports.getNote = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { id } = request.params;
  if(!id || id.length<4){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
    const note = await getNote_(id);
    const noteResponseFormat = {
      "_id": note._id,
      "userId": note.userId,
      "completed": note.completed,
      "text": note.text,
      "createdDate": note.createdDate
    }
    response.status(200).json(noteResponseFormat);
    return;
  } catch (e) {
    response.status(e.status).json({ message: e.message});
    return;
  }
};

module.exports.putNote = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { text } = request.body;
  const { id } = request.params;
  if(!id || !text || id.length<4 || text.length==0){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
    await updateNote_(id, text);
    response.status(200).json({ message: "Success"});
    return;
  } catch (e) {
    response.status(e.status).json({ message: e.message});
    return;
  }
};

module.exports.patchNote = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { id } = request.params;
  if(!id || id.length<4){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
    const note = await getNote_(id);
    if (note.completed) {
      await uncheckNote_(id);
    } else {
      await checkNote_(id);
    }
    response.status(200).json({ message: "Success" });
    return;
  } catch (e) {
    response.status(e.status).json({ message: e.message});
    return;
  }
};

module.exports.deleteNote = async (request, response) => {
  response.setHeader("Content-Type", "application/json");
  const { id } = request.params;
  if(!id || id.length<4){
    response.status(400).json({ message: "Query data is inappropriate"});
    return;
}
  try {
    await deleteNote_(id);
    response.status(200).json({ message: "Success"});
    return;
  } catch (e) {
    response.status(e.status).json({ message: e.message});
    return;
  }
};
