const {getUser, deleteUser, addUser, updateUser, getUserByUsernameAndPassword} = require("../dbcontroller/userDBController");
const sha256 = require('js-sha256').sha256;


module.exports.getUser = async (request, response) => {
    response.setHeader("Content-Type", "application/json");
    const { username, password } = request.body;
    if(!username || !password || username.length==0 || password.length==0){
        response.status(400).json({ message: "Query data is inappropriate"});
        return;
    }
    try{
        
        const user = await getUserByUsernameAndPassword(username, password);
        const userResponseFormat = {
                "_id": user._id,
                "username": user.username,
                "createdDate": user.createdDate
        }
        
        response.status(200).json(userResponseFormat);
        return;
    }catch(e){
        
        response.status(e.status).json({ message: e.message});
        return;
    }
};

module.exports.deleteUser = async (request, response) => {
    response.setHeader("Content-Type", "application/json");
    const { username, password } = request.body;
    if(!username || !password || username.length==0 || password.length==0){
        response.status(400).json({ message: "Query data is inappropriate"});
        return;
    }
    try{
        const user = await getUserByUsernameAndPassword(username, password);
        await deleteUser(user._id);
        response.status(200).json({message: 'Success'});
        return;
    }catch(e){
        response.status(e.status).json({ message: e.message});
        return;
    }
};

module.exports.patchUser = async (request, response) => {
    response.setHeader("Content-Type", "application/json");
    const { username, password , newPassword } = request.body;
    if(!username || !password || !newPassword || username.length==0 || password.length==0 || newPassword.length==0){
        response.status(400).json({ message: "Query data is inappropriate"});
        return;
    }
    const passwordHash = sha256(newPassword);
    
    try{
        const user = await getUserByUsernameAndPassword(username, password);
        await updateUser(user._id, passwordHash);
        response.status(200).json({message: 'Success'});
        return;
    }catch(e){
        response.status(e.status).json({ message: e.message});
        return;
    }
};
